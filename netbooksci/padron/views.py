from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from padron.models import Computadora
#Importo el formulario para crear nuevos estudiantes
from padron.forms import ComputadoraForm

# Create your views here.

def index(request):
	return render(request, 'padron/index.html')

"""def nuevacomputadora (request):
	if request.method=='POST':
		form = ComputadoraForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect ('padron_nets:index')
	else:
		form = ComputadoraForm()

	return render(request, 'padron/nuevacomputadora.html', {'form':form})"""


class ListaNets (ListView):
	model = Computadora
	template_name = 'padron/listanets.html'

class NuevaNet (CreateView):
	model = Computadora
	form_class = ComputadoraForm
	template_name = 'padron/nuevacomputadora.html'
	success_url = reverse_lazy('padron_nets:lista_nets')

class EditarNet (UpdateView):
	model = Computadora
	form_class = ComputadoraForm
	template_name = 'padron/nuevacomputadora.html'
	success_url = reverse_lazy('padron_nets:lista_nets')

class BorrarNet (DeleteView):
	model = Computadora
	template_name = 'padron/eliminarnet.html'
	success_url = reverse_lazy('padron_nets:lista_nets')
