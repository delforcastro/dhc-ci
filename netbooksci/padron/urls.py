from django.urls import include, path, re_path

from . import views

app_name = "padron_nets"
urlpatterns = [
				path('', views.index, name='index'),
				path('listacomputadoras', views.ListaNets.as_view(), name='lista_nets'),
				path('nuevacomputadora', views.NuevaNet.as_view(), name='nueva_net'),
				#el re_path lo saqué de esta documentación oficial https://docs.djangoproject.com/en/3.0/topics/http/urls/
				re_path(r'^editarnet/(?P<pk>\d+)/$', views.EditarNet.as_view(), name='editar_net'),
				re_path(r'^eliminarnet/(?P<pk>\d+)/$', views.BorrarNet.as_view(), name='eliminar_net'),
]
