from django import forms

from padron.models import Computadora
class ComputadoraForm (forms.ModelForm):
    class Meta:
        model = Computadora

        fields = [
        'estudiante',
        'serie',
        'idhardware',
        'observacion',
        ]

        labels = {
        'estudiante':'Estudiante',
        'serie' : 'N° de Serie',
        'idhardware':'ID de Hardware',
        'observacion':'Observaciones',
         }

        widgets = {
         'estudiante' :forms.Select(attrs={'class':'form-control'}),
         'serie': forms.TextInput(attrs={'class':'form-control'}),
         'idhardware' : forms.TextInput(attrs={'class':'form-control'}),
         'observacion' : forms.TextInput(attrs={'class':'form-control'}),
         }
