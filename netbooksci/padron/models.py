from django.db import models
from estudiantes.models import Estudiante

class Computadora(models.Model):

	estudiante = models.OneToOneField(Estudiante, null=True, blank=True, on_delete=models.CASCADE)
	serie = models.CharField(max_length=20)
	idhardware = models.CharField(max_length=20)
	observacion = models.TextField()

	def __str__(self):
		return self.serie
