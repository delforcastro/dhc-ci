from django.urls import include, path, re_path

from . import views
app_name = "estudiantes"

urlpatterns = [
				path('', views.index, name='index'),
				path('listaestudiante', views.ListaEstudiantes.as_view(), name='lista_estudiante'),
				path('nuevoestudiante', views.NuevoEstudiante.as_view(), name='nuevo_estudiante'),
				#el re_path lo saqué de esta documentación oficial https://docs.djangoproject.com/en/3.0/topics/http/urls/
				#re_path(r'^editarestudiante/(?P<id_estudiante>\d+)/$', views.editarestudiante, name='editar_estudiante'),
				re_path(r'^editarestudiante/(?P<pk>\d+)/$', views.EditarEstudiante.as_view(), name='editar_estudiante'),
				#re_path(r'^eliminarestudiante/(?P<pk>\d+)/$', views.EliminarEstudiante.as_view(), name='eliminar_estudiante'),
				#re_path(r'^eliminarestudiante/(?P<id_estudiante>\d+)/$', views.eliminarestudiante, name='eliminar_estudiante'),
				re_path(r'^eliminarestudiante/(?P<pk>\d+)/$', views.BorrarEstudiante.as_view(), name='eliminar_estudiante'),

]
