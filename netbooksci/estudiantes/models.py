from django.db import models

#Creo el modelo de estudiante

class Estudiante (models.Model):

	#defino los cursos
	#se sigue la estructura año-división (aXdX)
	a1d1 = 'a1d1'
	a1d2 = 'a1d2'
	a1d3 = 'a1d3'
	a1d4 = 'a1d4'
	a1d5 = 'a1d5'
	a2d1 = 'a2d1'
	a2d2 = 'a2d2'
	a2d3 = 'a2d3'
	a2d4 = 'a2d4'
	a2d5 = 'a2d5'
	a3d1 = 'a3d1'
	a3d2 = 'a3d2'
	a3d3 = 'a3d3'
	a3d4 = 'a3d4'
	a3d5 = 'a3d5'

	a4dnat1 = 'a4dnat1'
	a4dnat2 = 'a4dnat2'
	a5dnat1 = 'a5dnat1'
	a5dnat2 = 'a5dnat2'
	a6dnat = 'a6dnat'

	a4dc = 'a4dc'
	a5dc = 'a5dc'
	a6dc = 'a6dc'

	a4dt = 'a4dt'
	a5dt = 'a5dt'
	a6dt = 'a6dt'

	a4dh = 'a4dh'
	a5dh = 'a5dh'
	a6dh = 'a6dh'

	egre = 'egre'

	denominacion_cursos = [
		(a1d1,'1° Año 1a. División'),
		(a1d2,'1° Año 2a. División'),
		(a1d3,'1° Año 3a. División'),
		(a1d4,'1° Año 4a. División'),
		(a1d5,'1° Año 5a. División'),
		(a2d1,'2° Año 1a. División'),
		(a2d2,'2° Año 2a. División'),
		(a2d3,'2° Año 3a. División'),
		(a2d4,'2° Año 4a. División'),
		(a2d5,'2° Año 5a. División'),
		(a3d1,'3° Año 1a. División'),
		(a3d2,'3° Año 2a. División'),
		(a3d3,'3° Año 3a. División'),
		(a3d4,'3° Año 4a. División'),
		(a3d5,'3° Año 5a. División'),
		(a4dnat1,'4° Año Naturales 1° División'),
		(a4dnat2,'4° Año Naturales 2° División'),
		(a5dnat1,'5° Año Naturales 1° División'),
		(a5dnat2,'5° Año Naturales 2° División'),
		(a6dnat,'6° Año Naturales'),
		(a4dc,'4° Año Comunicación'),
		(a5dc,'5° Año Comunicación'),
		(a6dc,'6° Año Comunicación'),
		(a4dt,'4° Año Turismo'),
		(a5dt,'5° Año Turismo'),
		(a6dt,'6° Año Turismo'),
		(a4dh,'4° Año Humanidades'),
		(a5dh,'5° Año Humanidades'),
		(a6dh,'6° Año Humanidades'),
		(egre,'EGRESADO'),
	]

	apellido = models.CharField(max_length=60)
	nombre = models.CharField(max_length=150)
	curso = models.CharField(max_length=10, choices=denominacion_cursos, default=a1d1)
	dni = models.IntegerField()
	email = models.EmailField()
	fono = models.CharField(max_length=20)

	def __str__(self):
		return self.apellido + ", " + self.nombre
