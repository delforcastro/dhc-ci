from django import forms

from estudiantes.models import Estudiante
class EstudianteForm (forms.ModelForm):
    class Meta:
        model = Estudiante

        fields = [
        'apellido',
        'nombre',
        'curso',
        'dni',
        'email',
        'fono',
        ]

        labels = {
        'apellido':'Apellido',
        'nombre' : 'Nombres',
        'curso':'Curso actual',
        'dni':'N° DNI',
        'email':'Correo electrónico',
        'fono':'Teléfono de contacto',
         }

        widgets = {
         'apellido':forms.TextInput(attrs={'class':'form-control'}),
         'nombre': forms.TextInput(attrs={'class':'form-control'}),
         'curso' : forms.Select(attrs={'class':'form-control'}),
         'dni' : forms.TextInput(attrs={'class':'form-control'}),
         'email' : forms.TextInput(attrs={'class':'form-control'}),
         'fono' : forms.TextInput(attrs={'class':'form-control'}),
         }
