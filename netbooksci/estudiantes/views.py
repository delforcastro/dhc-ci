from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
#Importo el formulario para crear nuevos estudiantes
from estudiantes.forms import EstudianteForm


#Importo el modelo Estudiante

from estudiantes.models import Estudiante
# Create your views here.

def index(request):
	return render(request, 'estudiantes/index.html')

"""def nuevoestudiante (request):
	if request.method=='POST':
		form = EstudianteForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect ('estudiantes:index')
	else:
		form = EstudianteForm()

	return render(request, 'estudiantes/nuevoestudiante.html', {'form':form})"""


#La función listaestudiante fue para probar crear la vista usando funciones
#Queda sin uso porque después empecé a usar herencia de ListView

"""	def listaestudiante (request):
		estudiante = Estudiante.objects.all().order_by('apellido')
		#Acá creo un diccionario que llamaré 'estudiantes', así que
		#en la template preguntaré por 'estudianteS' y NO 'estudiantE'
		conjunto = {'estudiantes':estudiante}

		#El conjunto lo envío como contexto al template
		return render (request, 'estudiantes/listaestudiante.html', conjunto)"""

"""def editarestudiante (request, id_estudiante):
	estudiante = Estudiante.objects.get(id=id_estudiante)
	if request.method =='GET':
		form = EstudianteForm(instance=estudiante)
	else:
		form = EstudianteForm(request.POST, instance=estudiante)
		if form.is_valid():
			form.save()
		return redirect('estudiantes:lista_estudiante')
	return render(request, 'estudiantes/nuevoestudiante.html', {'form':form})"""

"""def eliminarestudiante(request, id_estudiante):
	estudiante = Estudiante.objects.get(id=id_estudiante)
	if request.method == 'POST':
		estudiante.delete()
		return redirect ('estudiantes:lista_estudiante')
	return render(request, 'estudiantes/eliminarestudiante.html', {'estudiante':estudiante})"""

class ListaEstudiantes (ListView):
	model = Estudiante
	template_name = 'estudiantes/listaestudiante.html'

class NuevoEstudiante (CreateView):
	model = Estudiante
	form_class = EstudianteForm
	template_name = 'estudiantes/nuevoestudiante.html'
	success_url = reverse_lazy('estudiantes:lista_estudiante')

class EditarEstudiante (UpdateView):
	model = Estudiante
	form_class = EstudianteForm
	template_name = 'estudiantes/nuevoestudiante.html'
	success_url = reverse_lazy('estudiantes:lista_estudiante')

class BorrarEstudiante (DeleteView):
	model = Estudiante
	template_name = 'estudiantes/eliminarestudiante.html'
	success_url = reverse_lazy('estudiantes:lista_estudiante')
